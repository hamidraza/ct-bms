import React, {useState} from 'react';
import './App.css';
import SeatRow from './seats-row';
const seatingData = require('./data/seats.json');

function App() {
  const [totalSeats, setTotalSeats] = useState(6);
  const [selectedSeats, setSelectedSeats] = useState([]);
  const [activeSeatType, setActiveSeatType] = useState(1);
  const seatsData = seatingData.data;

  const handleSeatSelection = ({type, row, seatIndex}) => {
    let tempSeats = selectedSeats;
    let remainingSeatsToSelect = totalSeats - selectedSeats.length;
    if(remainingSeatsToSelect <= 0 || !activeSeatType || type !== activeSeatType) {
      setActiveSeatType(type);
      tempSeats = [];
      setSelectedSeats(tempSeats);
      remainingSeatsToSelect = totalSeats;
    }
    // console.log('remainingSeatsToSelect', remainingSeatsToSelect);
    const seatGroup = seatsData.find(s => s.id === type);
    const currentRow = seatGroup.seats[row];
    // debugger;
    for(let i = seatIndex; i < Math.min(seatIndex + remainingSeatsToSelect, currentRow.length); i++) {
      const seat = currentRow[i];
      if(seat.status === 1) {
        tempSeats = [...tempSeats, {row, seat: i}];
        // debugger;
        // if(tempSeats.length >= remainingSeatsToSelect) break;
      } else {
        break;
      }
    }
    setSelectedSeats(tempSeats);
  }

  return (
    <div className="App">
      <div>
        <input type='range' min='1' max='10' value={totalSeats} onChange={({target}) => setTotalSeats(target.value)} />
        {totalSeats}
      </div>
      <div>
        {seatsData.map(data => {
          return (
            <div key={data.id} className='seat-group'>
              <h3>{data.title}</h3>
              {data.seats.map((row, i) => (
                <SeatRow
                  activeSeatType={activeSeatType}
                  seatType={data.id}
                  rowIndex={i}
                  seats={row}
                  key={i}
                  selectedSeats={selectedSeats}
                  onClick={seatIndex => handleSeatSelection({type: data.id, row: i, seatIndex})}
                />
              ))}
            </div>
          );
        })}
      </div>
    </div>
  );
}

export default App;
