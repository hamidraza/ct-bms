import React from 'react';
import './seats-row.css';

const seatStatus = {
  0: 'disabled',
  1: 'available',
  2: 'booked',
};

const SeatRow = ({
  seatType = null,
  activeSeatType = null,
  rowIndex = null,
  selectedSeats = [],
  seats = [],
  onClick = () => {},
}) => {

  return (
    <div className='seat-row'>
    {seats.map((s, i) => {
      const isSelected = selectedSeats.some(s => s.row === rowIndex && s.seat === i && seatType === activeSeatType);
      return (
        <div key={i} className={`seat-item ${isSelected ? 'seat-item-selected':''}`}>
          <div
            className={`seat-item-content seat-status-${seatStatus[s.status]}`}
            onClick={() => onClick(i)}
          >
            {s.status ? i+1:''}
          </div>
        </div>
      );
    })}
    </div>
  )
};

export default SeatRow;
